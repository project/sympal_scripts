<?php
require_once("lib/pcltrace.lib.php");
require_once("lib/pcltar.lib.php");
require_once("lib/FileUtils.php");

$cacheDirectory="./cache";
$moduleDirectory="./cache";
$modulesCacheFile=$cacheDirectory."/modules.tmp";
$cleanOpt=true;
$updateOpt=true;
$downloadOpt=false;
$installOpt=false;
$listOpt=false;

$downloadArrayOpt=array();
$installArrayOpt=array();
$searchArrayOpt=array();
$infoArrayOpt=array();
$infoModuleName="";

$searchOpt=false;
$infoOpt=false;
$verboseOpt=true;
$helpOpt=false;

if ($argc > 1) {
	$verboseOpt=!array_search("--quiet", $argv);	
	$cleanOpt=array_search("--clean", $argv);	
	$helpOpt=array_search("--help", $argv);	
	$updateOpt=array_search("--update", $argv);	
	$downloadOpt=array_search("--download", $argv);	
	$listOpt=array_search("--list", $argv);	
	if ($downloadOpt) {
		for ($i=$downloadOpt+1; ($i<count($argv))&& ($argv[$i][0] <> '-')  ;$i++) {
			if ($verboseOpt) echo "Position :".$i." values :".$argv[$i]."\n";
			array_push($downloadArrayOpt, $argv[$i]);
		}
	}

	$installOpt=array_search("--install", $argv);	
	if ($installOpt) {
		for ($i=$installOpt+1; ($i<count($argv))&& ($argv[$i][0] <> '-')  ;$i++) {
			if ($verboseOpt) echo "Position :".$i." values :".$argv[$i]."\n";
			array_push($installArrayOpt, $argv[$i]);
		}
	}

	$searchOpt=array_search("--search", $argv);	
	if ($searchOpt) {
		for ($i=$searchOpt+1; ($i<count($argv))&& ($argv[$i][0] <> '-')  ;$i++) {
			if ($verboseOpt) echo "Position :".$i." values :".$argv[$i]."\n";
			array_push($searchArrayOpt, $argv[$i]);
		}
	}

	$infoOpt=array_search("--info", $argv);	
	if ($infoOpt) {
		for ($i=$infoOpt+1; ($i<count($argv))&& ($argv[$i][0] <> '-')  ;$i++) {
			if ($verboseOpt) echo "Position :".$i." values :".$argv[$i]."\n";
			array_push($infoArrayOpt, $argv[$i]);
		}
	}

	if ($verboseOpt) echo "Argument detected...\n";
} else {
	if ($verboseOpt) echo "No Argument detected...\n";
	$helpOpt=true;
}

if ($helpOpt) {
	if ($verboseOpt) echo "Display Help...\n";
	displayHelp();
	return;
}
if ($searchOpt) {
	echo "Searching .....";
	return;
}

if ($infoOpt) {
	echo "Display info for module .....";
	return;
}

if ($cleanOpt) cleanCache();

if ($updateOpt) updateModuleInformation(); 

if ($downloadOpt) downloadModules($downloadArrayOpt); 

if ($installOpt)  installModules($installArrayOpt); 

if ($listOpt) listModules();

function listModules($cache=null) {
	if (!isset($cache)) {
		global $modulesCacheFile;
		$cache=$modulesCacheFile;
	}
	echo "Cache : $cache\n";
	$aModuleInfo=getModuleInfoFromCache($cache);	
	$i=1;
	foreach ($aModuleInfo as $modulename => $content) {
		if ($modulename == "") continue;
		echo "$i) \"$modulename\" \n";
		$i++;
	}
	return;
}

function installModules($modules, $cache=null) {
		if (!isset($cache)) {
		global $modulesCacheFile;
		$cache=$modulesCacheFile;
	}
	global $moduleDirectory;
	
	if (in_array("all", $modules)) {
		installAllModules(getModuleInfoFromCache($cache), $cacheDirectory);
	} else {
		foreach ($modules as $modulename) {
			echo "Try to install \"$modulename\" ...\n";
			installModule($modulename, $moduleDirectory);		
		}	
	}
}

function downloadModules($modules, $cache=null) {
	if (!isset($cache)) {
		global $modulesCacheFile;
		$cache=$modulesCacheFile;
	}
	global $cacheDirectory;
	
	$aModuleInfo=getModuleInfoFromCache($cache);	

	if (in_array("all", $modules)) {
		downloadAllModules($aModuleInfo, $cacheDirectory);
	} else {
		foreach ($modules as $modulename) {
			echo "Try to download \"$modulename\" ...\n";
			downloadModule($aModuleInfo, $modulename, $cacheDirectory);		
		}	
	}
}

function downloadAllModules($aModuleInfo, $cache) {
	echo "download all modules... ;o) no no no\n";
		foreach ($aModuleInfo as $modulename => $content) {
			if ($modulename == "") continue;
			echo "Try to download \"$modulename\" ...\n";
			downloadModule($aModuleInfo, $modulename, $cache);		
		}
	return;
}

function installAllModules($aModuleInfo, $cache) {
	echo "install all modules... ;o) no no no\n";
		foreach ($aModuleInfo as $modulename => $content) {
			if ($modulename == "") continue;
			echo "Try to install \"$modulename\" ...\n";
			installModule( $modulename, $cache);		
		}
	return;
}
function downloadModule ($aModuleInfo, $modulename, $cacheDirectory)
{
 if (!isset($aModuleInfo["$modulename"])) {
 	echo "No Module \"$modulename\" found in cache.\n";
	return;
 }
 
 $file=$cacheDirectory."/".$modulename.".tar.gz";
 echo "Fetching \"$modulename\" for drupal.org.\n"; 
 global $UA;

       if (file_exists($file)) unlink($file);
       
       $fp = fopen($file, "w");
       if ($fp==false) 
       { 
       	print "\nERROR OPEN WRITE : $file\n"; 
       	return;
       	}
       $ch = curl_init($aModuleInfo["$modulename"]["moduleDownloadUrl"]);
       curl_setopt($ch, CURLOPT_FILE, $fp);
       if ($proxyHost!="") curl_setopt($ch, CURLOPT_PROXY, "$proxyHost:$proxyPort"); 
       curl_setopt($ch, CURLOPT_USERAGENT, $UA);
       echo "Fetching url : ".$aModuleInfo["$modulename"]["moduleDownloadUrl"]." => ".$file."\n";
	curl_exec($ch);
	
	curl_close($ch);
	fclose($fp);	
	
//	echo "Unarchive Module in cache directory\n";
//	PclTarExtract($file, $cacheDirectory);	

}

function installModule ( $modulename, $cacheDirectory)
{
	$file=$cacheDirectory."/".$modulename.".tar.gz";
 
	if (!file_exists($file)) {
		downloadModules(array($modulename));
	}
       
	echo "Unarchive Module in cache directory\n";
	PclTarExtract($file, $cacheDirectory);	
}



function displayHelp() {
	global $argv;
?>
This is a command line PHP script with four main options.

  Usage:
  <?php echo $argv[0]; ?> <option>

  <option> 
	--clean    			: flush the module cache file.
	--update   			: update modules information cache.
	--download all|<modulename>	: Download in cache the last version of a module
	--install <modulename> (all|site_name, ...)	: Install a module for all the sites or for specified site 
	--info <modulename> 		: display module information for <modulename> module
	--search <searchstring>		: Search a module for a search string 
	--quiet				: Disable display verbose information 
	--list				: List the module available in cache 
	--help				: Display this help 
<?php
}

function cleanCache($cache=null) {
	if (!isset($cache)) {
		global $cacheDirectory;
		$cache=$cacheDirectory;
	}
	echo "Cleaning cache \"$cache\"...\n";
	if (is_dir($cache)) {
		recursive_remove_directory($cache);
		echo "Cache flushed.\n";
		return;
	}
	echo "No cache detected.\n";
}

function updateModuleInformation($version="4.7") {
	global $modulesCacheFile;
	echo "Retrieve modules list.\n";
	$rawHtmlModuleInfo = getDrupalModulesInfo($version);
	echo "Normalize module information.\n";
	$aModuleInfo = normalizeModuleInfo($rawHtmlModuleInfo);

	echo "Caching information.\n";
	cacheModuleInfo($aModuleInfo, $modulesCacheFile);
}

function serializeModuleInfo($array) {
	$data="";

	foreach ( $array as $name => $content) {
		$data.="$name"."\t;\t";
		$data.=$content["description"]."\t;\t";
		$data.=$content["urlModuleProject"]."\t;\t";
		$data.=$content["moduleDownloadUrl"];
		$data.="\n";

	}
	return $data;
}


function unserializeModuleInfo($data) {
	$arrRes= array();

	$lines=split ("\n", $data);

	foreach ($lines as $line) {
		$arrTmp=split ("\t;\t", $line);

		$arrRes[$arrTmp[0]]= array( 
			"description" => $arrTmp[1],
			"urlModuleProject" => $arrTmp[2],
			"moduleDownloadUrl" => $arrTmp[3]);
	}
	return $arrRes;
}
function getModuleInfoFromCache($cache=null)  {
	if (!isset($cache)) {
		global $modulesCacheFile;
		$cache=$modulesCacheFile;
	}
	if (!file_exists($cache)) updateModuleInformation($cache);
	$data=file_get_contents($cache);
	return unserializeModuleInfo($data);
}

function cacheModuleInfo ($aModuleInfo, $cache)
{
	cleanCache($cache);

	echo "Caching .......\n";
	$data = serializeModuleInfo($aModuleInfo);

	global $cacheDirectory;
	if (!file_exists($cacheDirectory)) mkdir ($cacheDirectory);
	$fp = fopen($cache, "w");
	if ($fp==false) 
	{ 
		print "\nERREUR OPEN WRITE : $cache\n"; 
		return;
	}


	if (fwrite($fp, $data) === FALSE) {

		fclose($fp);

		unlink($cache);       
		return;
	}

	fclose($fp);
}


function getDrupalModulesInfo( $version="4.7", $proxyHost="", $proxyPort="") {	
	$modulesPage="http://drupal.org/project/Modules";
	$UA="ModuleFetcher/0.1";

	$post_data="edit[rid]=".$version."&op=Go&edit[form_id]=project_version_filter_form";

	$ch = curl_init();

	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	if ($proxyHost!="") curl_setopt($ch, CURLOPT_PROXY, "$proxyHost:$proxyPort"); 
	curl_setopt($ch, CURLOPT_USERAGENT, $UA);
	#	curl_setopt($ch, CURLOPT_VERBOSE, 1);
	curl_setopt($ch, CURLOPT_URL, $modulesPage."?".$post_data);

	$pageContent = trim(curl_exec($ch));
	curl_close($ch);

	return $pageContent;
}


function normalizeModuleInfo($rawData) {
	$arrModules=array();
	$arrTmpData=array();

	$rawDataCleaned = str_replace("\n", "", $rawData, $count);

	preg_match_all("/<div class=\"project-item project-modules .*?><h2><a href=\"(.*?)\">(.*?)<\/a><\/h2>(<.*?>)<div class=\"links\"><a href=\"(.*?)\">Download/", $rawDataCleaned, $arrTmpData); 
	//Normalize description
	for ($i=0;$i<count($arrTmpData[2]);$i++) {
		
		$arrModules[$arrTmpData[2][$i]]=array( 
			"description" => normalizeDescription($arrTmpData[3][$i]),
			"urlModuleProject" =>  $arrTmpData[1][$i],
			"moduleDownloadUrl" => $arrTmpData[4][$i]		
		);
	}
	
	return $arrModules;	
}

function normalizeDescription($descr)
{
	$search = array ('@<script[^>]*?>.*?</script>@si', // Supprime le javascript
		'@<[\/\!]*?[^<>]*?>@si',          // Supprime les balises HTML
		'@([\r\n])[\s]+@',                // Supprime les espaces
		'@&(quot|#34);@i',                // Remplace les entit�s HTML
		'@&(amp|#38);@i',
		'@&(lt|#60);@i',
		'@&(gt|#62);@i',
		'@&(nbsp|#160);@i',
		'@&(iexcl|#161);@i',
		'@&(cent|#162);@i',
		'@&(pound|#163);@i',
		'@&(copy|#169);@i',
		'@&#(\d+);@e');                    // Evaluation comme PHP

	$replace = array ('',
		'',
		'\1',
		'"',
		'&',
		'<',
		'>',
		' ',
		chr(161),
		chr(162),
		chr(163),
		chr(169),
		'chr(\1)');

	return preg_replace($search, $replace, $descr);
}
?>
