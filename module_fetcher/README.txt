moduleFetcher.php README file
-----------------------------
19/06/2006 :

HOW TO INSTALL IT :
create a directory 'cache' to store module meta data and fetched modules.

HOW TO USE IT :
This utility is mainly inspired from glorious ’apt-get’ from Debian project.

The main features are :

    * Automatic module installation
    * Download module
    * Maintain cache info and module
    * Search utilities included

This is a command line PHP script with four main options.

Usage :
drupal.php option

option
— clean : flush the module cache file.
— update : update modules information cache.
— download all| : Download in cache the last version of a module
— install (all|site_name, ...) : Install a module for all the sites or for specified site
— info : display module information for module
— search : Search a module for a search string
— quiet : Disable display verbose information
— list : List the module available in cache
— help : Display this help 
