<?php
/**
 * @file: Script to parse a Drupal script template. Examples are in druplaroot/templates/
 */
function print_help() {
  global $argv;
  print "usage: $argv[0] name=value name2=value2 ... nameN=valueN\n";
  print "required variables are: \n";
  foreach (get_required() as $variable) {
    print "\t$variable->name (example: $variable->name=$variable->example)\n";
  }
  print "optional variables are: \n";
  foreach (get_optional() as $variable) {
    print "\t$variable->name (example: $variable->name=$variable->example)\n";
  }
  exit;
}

/**
 * Parses a template
 * @args: $filename: file name of the file to parse. Relative to drupal root
 *        $variables: array of variables. array('var1_name' => $var1_value, etc)
 * @return: the contents of a file that is parsed.
 */
function parse_file($file_name, $variables) {
  $content = file_get_contents($file_name);
  foreach ($variables as $variable) {
    $search[$variable->name] = '@<token>\s?'. $variable->name .'\s?</token>@si';
    $replacement[$variable->name] = $variable->value;
  }
  $content = preg_replace($search, $replacement, $content);
  return $content;
}

/**
 * Report status messages to the console.
 * TODO Do some strlength and so to make the output a lot nicer
 */
function report_to_console($message, $type, $status) {
  if ($status) {
    $status = "[OK]";
  }
  else {
    $status = "[FALSE]";
  }
  $msg = "$type:\t\t$message\t\t$status\n";
  print $msg;
}

/**
 * Parses the argument list --name value pairs
 * @return an array containing all the variable names as key, and values as value.
 */
function parse_dash_arguments($name = '') {
  global $argv;
  $variables = array();
  foreach ($argv as $entry) {
    if (strpos($entry, '--') === 0) {
      $key = substr($entry, 2);
      $next = key($argv);
      $value = '';
      if ($argv[$next] && (strpos($argv[$next], '--') === false)) {
        $value = $argv[$next];
        next($argv);
      }
      $variables[$key] = $value;
    }
  }

  if ($name) {
    return $variables[$name];
  }

  return $variables;
}

/**
 * Parses the argument list name=value pairs
 * @return an array containing all the variable names as key, and values as value.
 */
function parse_arguments() {
  global $argv;
  $variables = implode('&',$argv);
  parse_str($variables, $output);
  unset($output[0]);
  return $output;
}

/**
 * Filters out all non registered arguments.
 * @return an array containing all the variable objects that got a value.
 */
function parse_and_filter_arguments() {
  $registered_arr = get_registered();
  $arguments = parse_arguments();
  $variables = array();
  foreach ($registered_arr as $registered) {
    if (in_array($registered->name, array_keys($arguments))) {
      $variables[$registered->name] = $registered;
      $variables[$registered->name]->value = $arguments[$registered->name];
    }
  }
  return $variables;
}

/**
 * Find out if all the required variables were filled.
 * @return an array containing all the variable objects that got a value.
 */
function are_all_required_filled() {
  $required = get_required();
  $arguments = parse_and_filter_arguments();
  foreach ($required as $variable) {
    if (!in_array($variable->name, array_keys($arguments))) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * register a variable;
 */
function register_var($name, $example, $required = TRUE) {
  global $registered;
  $registered[$name]->example = $example;
  $registered[$name]->name = $name;
  if ($required) {
    $registered[$name]->required = TRUE;
  }
}

/**
 * returns a list of registered variables
 */
function get_registered() {
  global $registered;
  return $registered;
}

/**
 *  Give a list of all required variables
 */
function get_required() {
  global $registered;
  $required = array();
  //prefill the required ones
  //$required = TemplateVariables::get_required();
  foreach ($registered as $name => $variable) {
    if ($variable->required) {
      $required[$name] = $variable;
    }
  }
  return $required;
}

/**
 *  Give a list of all optional variables
 */
function get_optional() {
  global $registered;
  $optional = array();
  //prefill the required ones
  //$required = TemplateVariables::get_required();
  foreach ($registered as $name => $variable) {
    if (!$variable->required) {
      $optional[$name] = $variable;
    }
  }
  return $optional;
}
/**
 * Register the core required variables
 */
function init_core() {
  register_var('domain', 'example.com', TRUE);
  register_var('mysql_username', 'root', TRUE);
  register_var('mysql_password', 'r007', TRUE);
  register_var('database_name', 'examplecom', TRUE);
  register_var('main_email', 'main_email@othersite.com', TRUE);
}

/**
 * Find all installed domains in the drupal installation at $drupal_dir
 **/
function list_all_domains($drupal_dir) {
  $files = glob($drupal_dir .'/sites/*', GLOB_ONLYDIR);

  $domains = array();

  foreach ($files as $file) {
    $domain = array_pop(explode('/', $file));
    if ($domain && ($domain != 'CVS') && ($domain != 'default') &&
        file_exists($file .'/settings.php')) {
      $domains[] = $domain;
    }
  }

  return $domains;
}
/**
 * Helper functions
 */
/**
 * PHP5 file function
 * TODO Do not create this function when running PHP5
 */
function file_put_contents($file_name, $contents) {
  if (!$handle = fopen($file_name, 'w')) {
    echo "Cannot open file for writing ($file_name)\n";
    exit;
  }
  // Write $somecontent to our opened file.
  if (fwrite($handle, $contents) === FALSE) {
    echo "Cannot write to file ($file_name)\n";
    exit;
  }
  fclose($handle);
}
?>
