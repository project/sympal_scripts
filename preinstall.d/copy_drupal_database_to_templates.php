<?php
/**
 * @file Script to copy the database.mysql file from Drupal
 */
global $template_dir, $drupal_dir;

$dest = $template_dir .'/database.mysql';
$source = $drupal_dir .'/database/database.mysql';
if (!file_exists($dest) && file_exists($source)) {
  copy($source, $dest);
}
?>