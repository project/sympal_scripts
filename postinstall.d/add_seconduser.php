<?php
/**
 * @file Script adds the owner to the database
 */

register_var('default_username', 'example.com', TRUE);
register_var('default_password', 'p@s5', TRUE);

global $template_dir, $drupal_dir;
if (are_all_required_filled()) {
  $variables = parse_and_filter_arguments();
  $user = new stdClass();
  //check for role information
  //first assume a list of roles this user should get;
  $guessed_roles = array(
    "'authenticated user'",
    "'%editor%'",
    "'%moderator%'",
    //add yours here, make sure to ad the ''. You can use SQL % wildcards.
    );
  $guessed_roles_query = implode(', ',$guessed_roles);
  $result = db_query('SELECT * FROM {role} ORDER BY name WHERE name in (%s)', $guessed_roles_query);

  while ($role = db_fetch_object($result)) {
    $account['roles'][$role->rid] = $role->name;
  }

  $user->name = $variables['default_username']->value;
  $user->pass = $variables['default_password']->value;
  $user->mail = $variables['main_email']->value;
  $account = user_save($user, $account);

  //and mail the user his/her new details.
  $mail_variables = array(
    '%username' => $account->name,
    '%site' => variable_get('site_name', 'drupal'),
    '%password' => $account->pass,
    '%uri' => base_path(),
    '%uri_brief' => substr(base_path(), strlen('http://')),
    '%mailto' => $account->mail,
    '%date' => format_date(time()),
    '%login_uri' => url('user', NULL, NULL, TRUE),
    '%edit_uri' => url('user/'. $account->uid .'/edit', NULL, NULL, TRUE),
    '%login_url' => user_pass_reset_url($account)
    );
  $from = variable_get('site_mail', ini_get('sendmail_from'));
  $subject = _user_mail_text('welcome_subject', $mail_variables);
  $body = _user_mail_text('welcome_body', $mail_variables);
  user_mail($mail, $subject, $body, "From: $from\nReply-to: $from\nX-Mailer: Drupal\nReturn-path: $from\nErrors-to: $from");
}
else {
  print_help();
}
?>