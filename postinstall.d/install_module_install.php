<?php
/**
 * Script to insert all the .install database queries
 * TODO: Preg match against already existing tables
 */

/**
 * Helper function to include the .install files
 */
function include_all_install() {
  // Include install files for each module
  foreach (module_list() as $module) {
    $install_file = './'. drupal_get_path('module', $module) .'/'. $module .'.install';
    if (is_file($install_file)) {
      include_once $install_file;
      $install_files[$module] = $install_file;
    }
  }
  return $install_files;
}

/**
 * Helper function to run the hook_install from the .install files
 */
function invoke_all_hook_install($install_files) {
  foreach ($install_files as $module => $file) {
    $function = $module .'_install()';
    if (function_exists($function)) {
      $return =  call_user_func_array($function, $args)
  }
  return $return;
}

invoke_all_hook_install(include_all_install());
?>