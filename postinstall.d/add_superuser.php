<?php
/**
 * @file Script add a superuser to Drupal
 */
global $template_dir, $drupal_dir;

$superpass = md5(microtime() + rand());
//$superpass = 'very_secret';
//NOTE: you must change this for your situation. Also make sure that people cannot just read this script! Another good idea is to generate this one  @ random (default)

$user->uid = 1;
if(!$user = user_load($user)) {
  //NOTE: if a superuser exists, we reset its password and username!
  unset($user->uid);
}
$account['name'] = 'SuperUser';
$account['pass'] = $superpass;
$account['mail'] ='root@'. $argv[1];
user_save($user, $account);
?>