#!/usr/bin/php
<?php
# Script to install a new Drupal site from commandline in a multisite
/**
 * include the configuration settings.
 */
include_once('script_settings.php');

/**
 * include the parser
 */
include_once('common.php');

/**
 * Include all the .php files found at preinstall.php
 */
include_all('preinstall.d');

/**
 * Creates all the required directories for a new domain
 * TODO some form of feedback on success or failure would be nice.
 * TODO check for existing dirs, skip them.
 */
function create_directories($variables) {
  global $drupal_dir, $file_user, $file_group;
  $domain = $variables['domain']->value;
  $main_dir = $drupal_dir .'/sites/'. $domain;
  $dirs = array(
    $main_dir,
    $main_dir .'/themes',
    $main_dir .'/modules',
    $main_dir .'/files',
  );

  foreach ($dirs as $dir) {
    if(!file_exists($dir)) {
      $status = mkdir($dir, 0770);
      report_to_console($dir, 'Create', $status);
      $status = chown($dir, $file_user);
      report_to_console($dir, 'Set user', $status);
      $status = chgrp($dir, $file_group);
      report_to_console($dir, 'Set group', $status);
    }
    else {
      report_to_console($dir, 'Skip', 'Files exist');
    }
  }
}

/**
 * Creates all the required databases for a new domain
 */
function insert_data($variables) {
  global $template_dir, $sql_binary, $argv;

  //Find all database files
  $files = glob($template_dir .'/*.mysql');
  if (count($files) > 0) { //glob always returns an array
    foreach ($files as $file) {
      $contents[$file] = parse_file($file, $variables);
    }
    $content = implode("\n\n#################### $file ####################\n", $contents);
    //We now write a temporary database file to the temp dir.
    //file_put_contents cannot be used, because fclose destroys the tempfile.
    $tmpfname = tempnam("/tmp", "MYSQL_");
    file_put_contents($tmpfname, $content);

    //We now pipe this into mysql. Since we are writing a LOT of data, we use the client mysql for this, and not the database connection with PHP.
    //TODO: rewrite this part to make it use drupals internal database abstraction to write the data to,
    $cmd = escapeshellcmd($sql_binary .' -u '. $variables['mysql_username']->value .' -p'. $variables['mysql_password']->value .' '. $variables['database_name']->value) .' < '. escapeshellcmd($tmpfname);
    report_to_console($cmd, "Executing", TRUE);
    if (exec($cmd)) {
      report_to_console('Inserted database file', "Database", TRUE);
    }
    unlink($tmpfname); // this removes the file
  }
}

/**
 * Creates a new settings.php
 */
function create_settings($variables) {
  global $template_dir, $drupal_dir;
  $file_name = $template_dir .'/settings.php';

  $contents = parse_file($file_name, $variables);
  $file_name = $drupal_dir .'/sites/'. $variables['domain']->value .'/settings.php';
  $status = file_put_contents($file_name, $contents);
  report_to_console($file_name, 'Create', $status);
}

/**
 * Helper function to include the post and preinstall.d pluggable files
 */
function include_all($dir) {
  $files = glob($dir .'/*.php');
  if (count($files) > 0) { //glob always returns an array
    foreach ($files as $file) {
      include_once($file);
    }
  }
}

/**
 * This is where the action happens.
 * Three functions are always ran: to create the directories, create the settings file and insert the data. The rest is pluggable.
 */
init_core();
if (are_all_required_filled()) {
  $variables = parse_and_filter_arguments();
  create_directories($variables);
  create_settings($variables);
  insert_data($variables);
}
else {
  print_help();
}

/**
 * Include Drupal
 */
include_once('drupal.php');
installer_include_drupal();

/**
 * Include all the .php files found at preinstall.php
 */
include_all('postinstall.d');
?>
