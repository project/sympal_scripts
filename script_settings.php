<?php
/**
 * @file: Script to set the global environment for the scripts in Drupal
 */
global $template_dir, $drupal_dir, $sql_binary;
/**
 * Template dir is where the templates are stored. Should be relative to the drupal root.
 * Omit trailing slash.
 */
$template_dir = './templates';

/**
 * Drupal dir is the dir where you have drupal running.
 * It is adviced not to put these scripts in a place where apache can exectue them
 * NOTE: no trailing slash!
 */
$drupal_dir = '/var/www/drupal';

/**
 * location of mysql
 * invoke "which mysql" on the commandline to find the binary.
 * TODO make this postgre and mssql friendly
 */
$sql_binary = 'mysql';

/**
 * User settings
 * Define the user and group that become owner of the direcories and files.
 * TODO allow this as optional commandline parameter
 */
$file_user = 'www-data';
$file_group = 'www-data';
?>
