<?php
# Script to include Drupal in the CLI

/**
 * Include Drupal!
 */
function installer_include_drupal($site = NULL) {
  global $drupal_dir, $argv;
  unset($GLOBALS['base_url']);

  if ($site) {
    $_SERVER['HTTP_HOST'] = $site;
  }
  elseif ($site_arg = parse_dash_arguments('site')) {
    $_SERVER['HTTP_HOST'] = $site_arg;
  }
  else {
    return FALSE;
  }

  $php_self_tmp = $_SERVER['PHP_SELF'];
  $script_name_tmp = $_SERVER['SCRIPT_NAME'];

  $_SERVER['PHP_SELF'] = '/index.php'; //set the domain
  $_SERVER['SCRIPT_NAME'] = '/index.php';

  $cwd = getcwd();
  chdir($drupal_dir);
  include_once('includes/bootstrap.inc');
  //test if the domain is existing!
  if (function_exists('conf_init')) {
    if(strstr(conf_init(),'default')) {
      //We do not have a proer domain. And we should not test the default instalation on failure
      return FALSE;
    }
    //Drupal included sucessfull!
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    $here_i_am = $drupal_dir . base_path();
  }

  chdir($cwd);
  $_SERVER['PHP_SELF'] = $php_self_tmp;
  $_SERVER['SCRIPT_NAME'] = $script_name_tmp;

  return $here_i_am;
}

return installer_include_drupal();
?>