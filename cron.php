#!/usr/bin/php
<?php

function cron_init() {
  global $argc, $argv;
  if ($argc > 1) {
    include_once('script_settings.php');
    include_once('common.php');

    $arguments = parse_dash_arguments();

    if ($arguments['drupal_dir']) {
      $drupal_dir = $arguments['drupal_dir'];
    }
    if (isset($arguments['all'])) {
      /**  @todo: Sessions and variables are persistant. So this does not yet work **/
      return cron_run_all($drupal_dir);
    }
    elseif ($arguments['site']) {
      return cron_run_one($arguments['site'], $drupal_dir);
    }
  }
  return '';
}

function cron_run_one($site, $drupal_dir) {
  $sites = list_all_domains($drupal_dir);
  if (in_array($site, $sites)) {
    include_once('drupal.php');
    $message = installer_include_drupal($site);

    global $base_path, $base_url;

    report_to_console($base_url . $site, 'cron', $message);
    @fclose(@fopen("$base_url/cron.php" ,'r'));
  }
}

function cron_run_all($drupal_dir) {
  foreach (list_all_domains($drupal_dir) as $site) {
    cron_run_one($site, $drupal_dir);
  }
}
/**
 * Include Drupal
 */
cron_init();

?>