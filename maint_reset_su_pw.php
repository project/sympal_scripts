#!/usr/bin/php
<?php
# Script to install a new Drupal site from commandline in a multisite
function maint_reset_su_pw_print_help() {
  print "usage: maint_reset_supw.php domain password\n";
  exit;
}


/**
 * include the configuration settings.
 */
include_once('script_settings.php');

/**
 * include the parser
 */
include_once('common.php');

/**
 * This is where the action happens.
 * Three functions are always ran: to create the directories, create the settings file and insert the data. The rest is pluggable.
 * TODO we check for hardcoded variables. This should be made flexible, but kept secure....
 */
if (($domain = $argv[1]) &&
    ($password = $argv[2])) {
  /**
  * Include Drupal
  */
  include_once('drupal.php');
  installer_include_drupal($domain);
  db_query("UPDATE users SET pass = md5('%s')", $password);
  print "\nPassword is reset\n";
}
else {
  maint_reset_su_pw_print_help();
}

?>
