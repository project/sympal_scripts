#!/usr/bin/php
<?php
/**
 * Script to generate custom modules in the modules directory
 */
function print_help() {
  print "usage: generate_module.php domain type name\n";
  exit;
}


/**
 * include the configuration settings.
 */
include_once('script_settings.php');

/**
 * include the parser
 */
include_once('common.php');

/**
 * This is where the action happens.
 */
if (($domain = $argv[1]) &&
    ($type = $argv[2]) &&
    ($name = $argv[3])) {
  $variables = array(
     'domain' => $domain,
     'type' => $type,
     'name' => $name,
  );

  $file_name = $template_dir .'/'. $type .'.module';
  $contents = parse_file($file_name, $variables);

  if ($contents) {
    //Make the modules directory
    $dir = $drupal_dir .'/sites/'. $domain .'/modules/'. $name;
    $status = mkdir($dir, 0770);
    report_to_console($dir, 'Create', $status);

    if ($status) {
      //Make the module
      $file_name = $dir .'/'. $name .'.module';
      $status = file_put_contents($file_name, $contents);
      report_to_console($file_name, 'Create', $status);
    }
  }
}
else {
  print_help();
}
?>