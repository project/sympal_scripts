Sympal Scripts
By Bèr Kessels
sponored by sympal.nl
developed for sympal (sympal.nl)

These scripts are all PHP commandline scripts. They are inspired by the Ruby on Rails way of doing stuff. Simple agile commandline creation of code. No more copy-paste coding, but quick module generation.

It is developed with developers, consultants, package managers and hosting providers in mind. They should be able to use these scripts to install drupal from within their environment.
It is certainly not meant for Joe Weblog, for it requires one to run PHP commands on the server.

crontab.php
  A crontab file that runs all the cron.php for all the domains in sites/
  Usefull if you do not want to maintain loads of ever changing crontab entries.
install_site.php
  The installation script. Call this to install a new site. Call without arguments to see the help
common.php
  Common libary..
  Contains all common functions.
script_settings.php
  Here all the global settings are saved.
postinstall.d
preinstall.d
  Two directories to help the installer. All the scripts in there will be included (and thus run), respectively after and before the main installation script.
